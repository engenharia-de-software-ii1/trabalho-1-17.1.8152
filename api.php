<?php

    function somar ($num1, $num2){
        return $num1 + $num2;
    }

    function multiplicar($num1, $num2){
        return $num1 * $num2;
    }

    function verificar_soma ($num1, $num2, $num3){
        if (($num1 + $num2) + $num3 == $num1 + ($num2 + $num3))
            if ($num1 + $num2 == $num2 + $num1)
                if ($num1 + 0 == $num1 && $num2 + 0 == $num2)
                    if ($num1 + (-$num1) == 0 && $num2 + (-$num2) == 0)
                        return "Axiomas verdadeiros!";
                    else
                        return "Axioma de simetria falso!";
                else
                    return "Axioma de elemento neutro falso!";
            else
                return "Axioma de comutatividade falso!";
        else
            return "Axioma de associatividade falso!";
    }

    function verificar_produto ($num1, $num2, $num3){
        if ( ($num1 * $num2) * $num3 == $num1 * ($num2 * $num3) )
            if ($num1 * $num2 == $num2 * $num1)
                if ($num1 * 1 == $num1 && $num2 * 1 == $num2)
                    if ($num1 == 0 or $num2 == 0 or $num3 == 0)
                        return "Axioma falso pois uma das variaveis e igual a 0!";
                    else
                        if ($num1 * (1 / $num1) == 1 && $num2 * (1 / $num2) == 1 && $num3 * (1 / $num3) == 1)
                            return "Axiomas verdadeiros!";
                        else
                            return "Axioma de simetria falso!";
                else
                    return "Axioma de elemento neutro falso!";
            else
                return "Axioma de comutatividade falso!";
        else
            return "Axioma de associatividade falso!";
    }

    $data = json_decode(file_get_contents("php://input"), true);

    $num1 = 30;
    $num2 = 0;
    $num3 = 6.99;

    $soma = somar($num1, $num2);
    $mult = multiplicar($num1, $num2);
    $axioma_soma = verificar_soma($num1, $num2, $num3);
    $axioma_produto = verificar_produto($num1, $num2, $num3);

    $res = json_encode($axioma_produto);
    echo $res;

?>